/*
 * 13/04/2010
 */
package br.com.seinfra.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Classe utilitária que armazena todos os icones utilizados no sistema.
 * @author heber
 */
@Scope("application")
@Component("icons")
public final class IconesBean{

    /** IconesBean. */
    private Map<String, String> icons;
    /** Textos. */
    private Map<String, String> texts;
    /** Tipos/Style. */
    private Map<String, String> styles;

    /** Construtor. */
    @PostConstruct
    private void inicializar() {
        inicializarTexts();
        inicializarIcons();
        inicializarStyles();

    }

    private void inicializarIcons() {
        icons = new HashMap<>();
        icons.put("MAO_OBRA", "fa fa-users");
        icons.put("EQUIPAMENTO", "fa fa-screwdriver");
        icons.put("MATERIAL", "fa fa-box-open");
        icons.put("SERVICO", "fa fa-cogs");
        icons.put("OCORRENCIA", "fa fa-exclamation-triangle");
        icons.put("TRANSPORTE", "fa fa-truck");
        icons.put("ARQUIVO", "fa fa-images");
        icons.put("CATEGORIA", "fa fa-file-alt");

        icons.put("CLARO", "wi wi-day-sunny");
        icons.put("NUBLADO", "wi wi-cloudy");
        icons.put("CHUVOSO", "wi wi-rain");
        icons.put("VENTO_FORTE", "wi wi-strong-wind");
        icons.put("NEVANDO", "wi wi-snow");

        icons.put("ABERTO", "fa fa-edit");
        icons.put("PENDENTE", "fa fa-share-square");
        icons.put("REVISAR", "fa fa-ban");
        icons.put("APROVADO", "fa fa-check-square");

        icons.put("permissao", "fa fa-key");
        icons.put("senha", "fa fa-lock");
        icons.put("email", "fa fa-envelope");

        icons.put("usuarios", "fa fa-users");
        icons.put("camera", "fa fa-camera");
        icons.put("fotos", "fa fa-images");
        icons.put("limpar", "fa fa-erase");
        icons.put("info", "fa fa-info-circle");

        icons.put("adicionar", "fa fa-plus");
        icons.put("aprovar", "fa fa-check-square");
        icons.put("atualizar", "fa fa-sync");
        icons.put("cancelar", "fa fa-times");
        icons.put("enviar", "fa fa-share-square");
        icons.put("fechar", "fa fa-times");
        icons.put("duplicar", "fa fa-copy");
        icons.put("editar", "fa fa-edit");
        icons.put("excluir", "fa fa-trash");
        icons.put("limpar", "fa fa-eraser");
        icons.put("novo", "fa fa-plus");
        icons.put("rejeitar", "fa fa-ban");
        icons.put("salvar", "fa fa-save");

        icons.put("notificacao", "fa fa-bell");
        icons.put("exclamation", "fa fa-exclamation-triangle");

        icons.put("ok", "fa fa-check");
        icons.put("visualizar", "fa fa-search");
        icons.put("financeiro", "fa fa-dollar-sign");
        icons.put("config", "fa fa-cog");

        icons.put("relatorio", "fa fa-file-alt");

        icons.put("pdf", "fa fa-file-pdf");
        icons.put("excel", "fa fa-file-excel");
        icons.put("word", "fa fa-file-word");
        icons.put("zip", "fa fa-file-archive");

        icons.put("PDF", "fa fa-file-pdf");
        icons.put("DOCX", "fa fa-file-excel");
        icons.put("XLSX", "fa fa-file-word");
        icons.put("ZIP", "fa fa-file-archive");

        icons.put("DIARIO", "far fa-calendar-check");
        icons.put("MEDICAO", "fa fa-chart-line");

        icons.put("projeto", "fa fa-project-diagram");
        icons.put("diario", "far fa-calendar-check");
        icons.put("medicao", "fa fa-chart-line");
        icons.put("tarefas", "fa fa-list-ol");
        icons.put("cronograma", "fa fa-calendar-alt");
        icons.put("itens", "fa fa-list-ul");
        icons.put("bdi", "fa fa-percent");
        icons.put("grafico", "fa fa-chart-bar");
    }

    private void inicializarTexts() {
        texts = new HashMap<>();

        texts.put("atualizar", "Adicionar");
    }

    private void inicializarStyles() {
        styles = new HashMap<>();

        styles.put("alert", "alert-icon");
        styles.put("warning", "warning-icon");
        styles.put("success", "success-icon");
        styles.put("neutral", "neutral-icon");
        styles.put("default", "default-icon");

        styles.put("ERROR", "alert-icon");
        styles.put("WARNING", "warning-icon");
        styles.put("SUCCESS", "success-icon");
        styles.put("NEUTRAL", "neutral-icon");
        styles.put("DEFAULT", "default-icon");
    }

    public String getStyle(Object id) {
        return styles.get("" + id);
    }
    public String getIcon(Object id) {
        return icons.get("" + id);
    }
    public String getText(Object id) {
        return texts.get("" + id);
    }

    public String iniciar() {
        return null;
    }

    public String cancelar() {
        return null;
    }

}