package br.com.seinfra.repository.query;

import java.util.List;

import br.com.seinfra.model.Categoria;
import br.com.seinfra.model.filter.CategoriaFilter;

public interface CategoriaRepositoryQuery {

	public List<Categoria> pequisar(CategoriaFilter filtro);
	
	public Long total(CategoriaFilter filtro);
	
	public List<Categoria> todasCategorias();
	
}
