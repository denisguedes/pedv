package br.com.seinfra.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.seinfra.global.controller.GenericBaseBean;
import br.com.seinfra.model.Cliente;
import br.com.seinfra.model.filter.ClienteFilter;
import br.com.seinfra.service.ClienteService;

@Named
@ViewScoped
public class SelecaoClienteBean extends GenericBaseBean<ClienteService, Cliente, Long>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nome;
	
	private ClienteFilter filtro;
	
	private LazyDataModel<Cliente> model;
	
	public void pesquisar(){
		filtro = new ClienteFilter();
		limpar();
		
		model = new LazyDataModel<Cliente>() {
		private static final long serialVersionUID = 1L;
			
			@Override
			public List<Cliente> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
					Map<String, Object> filters) {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(servico.quantidadeFiltrados(filtro).intValue());
				
				return servico.pequisar(filtro);
			}
		};
	}
	
	private void limpar() {
		selecionado = new Cliente();
	}

	public void selecionar(Cliente cliente) {
		RequestContext.getCurrentInstance().closeDialog(cliente);
	}
	
	public void abrirDialogo() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("modal", true);
		opcoes.put("resizable", false);
		opcoes.put("height", 470);
		
		RequestContext.getCurrentInstance().openDialog("/dialogos/SelecaoCliente", opcoes, null);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LazyDataModel<Cliente> getModel() {
		return model;
	}

	public void setModel(LazyDataModel<Cliente> model) {
		this.model = model;
	}

}