package br.com.seinfra.model.enums;

public enum Periodo {

	MANHA("Manhã"),
	TARDE("Tarde"),
	NOITE("Noite");
	
	private final String descricao;
	
	Periodo(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
