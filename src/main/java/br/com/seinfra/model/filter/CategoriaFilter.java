package br.com.seinfra.model.filter;

import java.io.Serializable;

import br.com.seinfra.global.model.GenericBaseFilter;

public class CategoriaFilter extends GenericBaseFilter<Serializable>{

	private Long id;
	private String descricao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
