package br.com.seinfra.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Component;

import br.com.seinfra.global.controller.GenericBaseBean;
import br.com.seinfra.model.Cliente;
import br.com.seinfra.model.enums.TipoPessoa;
import br.com.seinfra.model.filter.ClienteFilter;
import br.com.seinfra.service.ClienteService;

@ViewScoped
@Component("clienteBean")
public class ClienteBean extends GenericBaseBean<ClienteService, Cliente, Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Set<TipoPessoa> tipoPessoa = new HashSet<>();
	
	private ClienteFilter filtro;
	
	private LazyDataModel<Cliente> model;
	
	  @PostConstruct
	    public void init() {
		  tipoPessoa = new HashSet<>();
			for(TipoPessoa tipo : TipoPessoa.values()) {
			tipoPessoa.add(tipo);
			}
	    }
	
	public ClienteBean() {
		setTipo("Cliente");
		filtro = new ClienteFilter();
		limpar();
		
		model = new LazyDataModel<Cliente>() {
		private static final long serialVersionUID = 1L;
			
			@Override
			public List<Cliente> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
					Map<String, Object> filters) {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(servico.quantidadeFiltrados(filtro).intValue());
				
				return servico.pequisar(filtro);
			}
		};
	}

	public void inicializar() {
		if (getSelecionado() == null) {
			limpar();
		}
	}
	
    public String novo() {
    	limpar();
        return null;
    }
	
	private void limpar() {
		selecionado = new Cliente();
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public ClienteFilter getFiltro() {
		return filtro;
	}

	public Set<TipoPessoa> getTipoPessoa() {
		return tipoPessoa;
	}

	public LazyDataModel<Cliente> getModel() {
		return model;
	}

	public void setModel(LazyDataModel<Cliente> model) {
		this.model = model;
	}

}
