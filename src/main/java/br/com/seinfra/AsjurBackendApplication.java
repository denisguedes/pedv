package br.com.seinfra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import br.com.seinfra.config.property.ConfigProperties;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(ConfigProperties.class)
@ComponentScan("br.com.seinfra")
public class AsjurBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsjurBackendApplication.class, args);
	}

}
