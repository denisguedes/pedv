package br.com.seinfra.model.filter;

import java.io.Serializable;

import br.com.seinfra.global.model.GenericBaseFilter;

public class ClienteFilter extends GenericBaseFilter<Serializable>{

	private Long id;
	private String nome;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}