package br.com.seinfra.global.model;

import java.io.Serializable;
import java.time.LocalDate;

public interface IGenericBaseModel {
	
	Serializable getId();
	
	LocalDate getDataCadastro();
	
	LocalDate getDataAlteracao();
	
}
