package br.com.seinfra.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;
import br.com.seinfra.model.enums.FormaPagamento;
import br.com.seinfra.model.enums.StatusPagamento;

@Entity
@Table(name = "tb_pagamento", schema = "bdestq")
public class Pagamento extends GenericBaseModel<Long> {

	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name = "data_vencimento")
	private LocalDate dataVencimento;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name = "data_pagamento")
	private LocalDate dataPagamento;
	
	@Column(name = "valor_parcela")
	private BigDecimal valorParcela;
	
	@NotNull
	@Column(name = "valor_pagamento")
	private BigDecimal valorPagamento;
	
	@Column(name = "numero")
	private Integer numero;
	
	@Column(name = "codigo_forma_pagamento")
	private FormaPagamento formaPagamento;
	
	@ManyToOne
	@JsonBackReference("pagamentos")
	@JoinColumn(name = "codigo_pedido")
	private Pedido pedido;
	
	@Column(name = "status")
	private StatusPagamento status;
	
	public Pagamento() {}

	public LocalDate getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public LocalDate getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(LocalDate dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public BigDecimal getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(BigDecimal valorParcela) {
		this.valorParcela = valorParcela;
	}

	public BigDecimal getValorPagamento() {
		return valorPagamento;
	}

	public void setValorPagamento(BigDecimal valorPagamento) {
		this.valorPagamento = valorPagamento;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public StatusPagamento getStatus() {
		return status;
	}

	public void setStatus(StatusPagamento status) {
		this.status = status;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append(dataVencimento).append(" - ")
				.append(valorParcela).append(" - ")
				.append(numero).toString();
	}
}
