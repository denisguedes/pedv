package br.com.seinfra.repository.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.seinfra.model.Cliente;
import br.com.seinfra.model.Pedido;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.enums.Periodo;
import br.com.seinfra.model.filter.PedidoFilter;
import br.com.seinfra.repository.query.PedidoRepositoryQuery;

public class PedidoRepositoryImpl implements PedidoRepositoryQuery {

	@Autowired
	private EntityManager manager;

	@Override
	public List<Pedido> pequisar(PedidoFilter filtro) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pedido> criteria = builder.createQuery(Pedido.class);
		Root<Pedido> root = criteria.from(Pedido.class);
		Join<Pedido, Cliente> joinCliente = root.join("cliente");

		Predicate[] predicates = criarRestricoes(filtro, builder, root, joinCliente);
		criteria.where(predicates);
		criteria.orderBy(builder.desc(root.get("dataCriacao")));

		TypedQuery<Pedido> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(PedidoFilter filtro, CriteriaBuilder builder,
			Root<Pedido> root, Join<Pedido, Cliente> joinCliente) {

		List<Predicate> predicates = new ArrayList<>();

		if (filtro.getNumeroDe() != null) {
			predicates.add(builder.equal(root.get("id"), filtro.getNumeroDe()));
		}

		if (filtro.getNumeroAte() != null) {
			predicates.add(builder.equal(root.get("id"), filtro.getNumeroAte()));
		}

		if (filtro.getDataCriacaoDe() != null) {
			predicates.add(builder.equal(root.get("dataCriacao"), filtro.getDataCriacaoDe()));
		}
		
		if (filtro.getDataCriacaoAte() != null) {
			predicates.add(builder.equal(root.get("dataCriacao"), filtro.getDataCriacaoAte()));
		}
		
		if (!StringUtils.isEmpty(filtro.getNomeCliente())) {
			predicates.add(builder.like(builder.lower(joinCliente.get("nome")),
					"%" + filtro.getNomeCliente().toLowerCase() + "%"));
		}	
		
		if (!StringUtils.isEmpty(filtro.getNomeCliente())) {
			predicates.add(builder.like(builder.lower(joinCliente.get("nome")),
					"%" + filtro.getNomeCliente().toLowerCase() + "%"));
		}	
		
		if (filtro.getStatuses() != null && filtro.getStatuses().length > 0) {
			predicates.add(root.get("status").in(filtro.getStatuses()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, PedidoFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(PedidoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Pedido> root = criteria.from(Pedido.class);
		Join<Pedido, Cliente> joinCliente = root.join("cliente");

		Predicate[] predicates = criarRestricoes(filtro, builder, root, joinCliente);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@Override
	public List<Pedido> verificaAgendamento(LocalDate dataInicio, LocalDate dataFim, Periodo periodo) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pedido> criteria = builder.createQuery(Pedido.class);
		Root<Pedido> root = criteria.from(Pedido.class);
		
			criteria.where(builder.or(
					builder.between(root.get("dataInicio"), dataInicio, dataFim),
					builder.between(root.get("dataFim"), dataInicio, dataFim)),
					builder.equal(root.get("periodo"), periodo));
		criteria.orderBy(builder.desc(root.get("dataPedido")));

		TypedQuery<Pedido> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public Map<Date, BigDecimal> valoresTotaisPorData(Integer numeroDeDias, Usuario criadoPor) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pedido> criteria = builder.createQuery(Pedido.class);
		Root<Pedido> root = criteria.from(Pedido.class);
		
		criteria.multiselect(builder.sum(root.get("valorTotal")), root.get("dataCriacao")).distinct(true);
		List<Predicate> predicates = new ArrayList<>();

		numeroDeDias -= 1;
		
		Calendar dataInicial = Calendar.getInstance();
		dataInicial = DateUtils.truncate(dataInicial, Calendar.DAY_OF_MONTH);
		dataInicial.add(Calendar.DAY_OF_MONTH, numeroDeDias * -1);
		
		Map<Date, BigDecimal> resultado = criarMapaVazio(numeroDeDias, dataInicial);
		
		if (criadoPor != null) {
			predicates.add(builder.equal(root.get("vendedor"), criadoPor));
		}
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.groupBy(root.get("dataCriacao"));
		
		TypedQuery<Pedido> query = manager.createQuery(criteria);
		
		for (Pedido dataValor : query.getResultList()) {
			resultado.put(dataValor.getDataCriacao(), dataValor.getValorTotal());
		}
		
		return resultado;
	}
	
	
	private Map<Date, BigDecimal> criarMapaVazio(Integer numeroDeDias,
			Calendar dataInicial) {
		dataInicial = (Calendar) dataInicial.clone();
		Map<Date, BigDecimal> mapaInicial = new TreeMap<>();

		for (int i = 0; i <= numeroDeDias; i++) {
			mapaInicial.put(dataInicial.getTime(), BigDecimal.ZERO);
			dataInicial.add(Calendar.DAY_OF_MONTH, 1);
		}
		
		return mapaInicial;
	}
	
}
