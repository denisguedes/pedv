package br.com.seinfra.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.filter.PermissaoFilter;
import br.com.seinfra.repository.PermissaoRepository;

@Service
public class PermissaoService  extends GenericBaseService<PermissaoRepository, Permissao, Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5974133964061573838L;

	public List<Permissao> pequisar(PermissaoFilter Filtro) {
		return repositorio.pequisar(Filtro);
	}
	
	public Long quantidadeFiltrados(PermissaoFilter filtro) {
		return repositorio.total(filtro);
	}
	
	public List<Permissao> findAll(){
		return repositorio.findAll();
	}
	
	public Permissao usuarioPermissao(String role) {
		return repositorio.findByRole(role);
	}
}
