package br.com.seinfra.repository.query;

import java.util.List;

import br.com.seinfra.model.Produto;
import br.com.seinfra.model.filter.ProdutoFilter;

public interface ProdutoRepositoryQuery {

	public List<Produto> pequisar(ProdutoFilter filtro);
	
	public Long total(ProdutoFilter filtro);
	
	public Produto porSku(String sku);
	
}
