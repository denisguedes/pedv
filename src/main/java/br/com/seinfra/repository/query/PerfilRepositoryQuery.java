package br.com.seinfra.repository.query;

import java.util.List;

import br.com.seinfra.model.Perfil;
import br.com.seinfra.model.filter.PerfilFilter;

public interface PerfilRepositoryQuery {

	public List<Perfil> pequisar(PerfilFilter filtro);
	
	public Long total(PerfilFilter filtro);
	
}
