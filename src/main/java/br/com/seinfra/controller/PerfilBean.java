 package br.com.seinfra.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.view.ViewScoped;

import org.primefaces.model.DualListModel;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.seinfra.config.jsf.FacesUtil;
import br.com.seinfra.global.controller.GenericBaseBean;
import br.com.seinfra.model.Perfil;
import br.com.seinfra.model.PerfilPermissao;
import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.filter.PerfilFilter;
import br.com.seinfra.service.PerfilService;
import br.com.seinfra.service.PermissaoService;

@ViewScoped
@Component("perfilBean")
public class PerfilBean extends GenericBaseBean<PerfilService, Perfil, Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PerfilFilter filtro;
	
	@Autowired
	private PermissaoService permissaoService;
	
	private DualListModel<Permissao> permissoes;
	
	private List<Permissao> listaPermissao;
	
	private List<Permissao> permissoesSelecionadas;
	
	private LazyDataModel<Perfil> model;
	
	public PerfilBean() {
		setTipo("Perfil");
		filtro = new PerfilFilter();
		model = new LazyDataModel<Perfil>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Perfil> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
					Map<String, Object> filters) {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(servico.quantidadeFiltrados(filtro).intValue());
				
				return servico.pequisar(filtro);
			}
		};
	}
	
	@Transactional
	public String salvar() {
		try {
			Set<PerfilPermissao> perfilPermissoes = new HashSet<>();
			for(Permissao permissao : permissoes.getTarget()) {
				PerfilPermissao perfilPermissao = new PerfilPermissao(getSelecionado(), permissao);
				perfilPermissoes.add(perfilPermissao);
			}
			getSelecionado().setPermissoes(perfilPermissoes);
			selecionado = servico.incluir(getSelecionado());
			
			FacesUtil.addInfoMessage("Registrado com sucesso!");
		} catch (Exception ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		}
		return "pesquisaPerfil";
	}

	public void inicializar() {
		listaPermissao = permissaoService.findAll();
		permissoesSelecionadas = new ArrayList<Permissao>();
		
		if (getSelecionado() == null) {
			limpar();
		}else {
			for(PerfilPermissao perfilPermissao : getSelecionado().getPermissoes()) {
				permissoesSelecionadas.add(perfilPermissao.getPermissao());
				listaPermissao.remove(perfilPermissao.getPermissao());
			}
		}
		permissoes = new DualListModel<Permissao>(listaPermissao, permissoesSelecionadas);
	}
	
	private void limpar() {
		selecionado = new Perfil();
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public PerfilFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(PerfilFilter filtro) {
		this.filtro = filtro;
	}

	public DualListModel<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(DualListModel<Permissao> permissoes) {
		this.permissoes = permissoes;
	}

	public LazyDataModel<Perfil> getModel() {
		return model;
	}

	public List<Permissao> getListaPermissao() {
		return listaPermissao;
	}

	public void setListaPermissao(List<Permissao> listaPermissao) {
		this.listaPermissao = listaPermissao;
	}

	public List<Permissao> getPermissoesSelecionadas() {
		return permissoesSelecionadas;
	}

	public void setPermissoesSelecionadas(List<Permissao> permissoesSelecionadas) {
		this.permissoesSelecionadas = permissoesSelecionadas;
	}

}
