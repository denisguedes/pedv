package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Endereco;
import br.com.seinfra.repository.query.EnderecoRepositoryQuery;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long>, EnderecoRepositoryQuery {

}
