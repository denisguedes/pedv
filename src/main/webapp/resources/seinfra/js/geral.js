/* ***************************************************************************************
 * AJAX - AJAX - AJAX - AJAX - AJAX - AJAX - AJAX - AJAX - AJAX - AJAX - AJAX - AJAX 
 *****************************************************************************************/
function afterAutoUpdate() {
    try { $.AdminLTE.layout.fix(); } catch(e) { log(e); }
    
    iniciar();
    
    //fakewaffle.responsiveTabs(['xs', 'sm']);
}

/* ***************************************************************************************
 * MENU - MENU - MENU - MENU - MENU - MENU - MENU - MENU - MENU - MENU - MENU - MENU
 *****************************************************************************************/

function desabilitarLinks() {
	$(".sidebar-menu li.disabled a").css("cursor", "not-allowed");
	$(".sidebar-menu li.disabled a").removeAttr("href");
	$(".sidebar-menu li.disabled a").removeAttr("onclick");
   	$(".sidebar-menu li.disabled a").click(function() { return false; });
   	
   	$(".nav-tabs li.disabled a").css("cursor", "not-allowed");
   	$(".nav-tabs li.disabled a").removeAttr("href");
   	$(".nav-tabs li.disabled a").removeAttr("onclick");
   	$(".nav-tabs li.disabled a").click(function() { return false; });
}

/* ***************************************************************************************
 * IMAGEM POPUP - IMAGEM POPUP - IMAGEM POPUP - IMAGEM POPUP - IMAGEM POPUP - IMAGEM POPUP
 *****************************************************************************************/

function getCaption(element) {
	var title = element.getAttribute('data-caption-title');
	var description = element.getAttribute('data-caption');
	
	var temTitle = title && title != '';
	var temDescription = description && description != '';
	
	return (temTitle ? '<b>' + title + '</b>' : '')
			+ (temTitle && temDescription ? '<br/>' : '')
			+ (temDescription ? description : '');
}

/* ***************************************************************************************
 * CALENDAR - CALENDAR - CALENDAR - CALENDAR - CALENDAR - CALENDAR - CALENDAR - CALENDAR
 *****************************************************************************************/

function construirCalendarios() {
	
	try {
        
		var cs = $("input.calendar");
		
		for (var i = 0; i < cs.length; i++ ) {
			var c = $(cs[i]);
			
			var single = 'true' == c.attr('data-single');
			var inicio = c.attr('data-inicio');
			var termino = c.attr('data-termino');
			var format = c.attr('data-format');
			var weekend = 'false' != c.attr('data-weekend-allowed');
			var ajaxEnabled = c.attr('data-ajax-enable');
			
			format = format ? format : 'DD/MM/YYYY';
			
	        c.daterangepicker({ showDropdowns: true, singleDatePicker: single, minDate: inicio, maxDate: termino,
        			locale: { format: format, firstDay: 0,
	        			applyLabel: "Aplicar", cancelLabel: "Cancelar", fromLabel: "De", toLabel: "Para",
	        			daysOfWeek: [ "D", "S", "T", "Q", "Q", "S", "S" ],
	        			monthNames: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
	        			              "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ], },
	        	    isInvalidDate: function(data) {
	        	    	if (!weekend) {
	        	    		var day = new Date(data).getDay();
	        	    		if (day == 0 || day == 6) {
	        	    			return true;
	        	    		}
	        	    	}
	        	    	return checkDatasPermitidas(data);
        	    	},
		        }, function(start, end, label) { });
	        
	        if (ajaxEnabled) {
		        c.on('apply.daterangepicker', function(ev, picker) {
		        		var id = ev.target.id;
		        		var form = ev.target.form ? ev.target.form.name : id.substring(0, id.indexOf(":"));
		        		var c = $(document.getElementById(id));
		        		var update = c.attr('data-ajax-update');
		        		update = update == '@form' ? form : update;
		        		var global = 'false' != c.attr('data-ajax-global');
		        		PrimeFaces.ab({s: ev.target, e: 'change', f: form, p: id, u: update, g: global } );
		        	});
	        }
		}
        
    } catch (e) { alert('calendar: ' + e); }
}


var datasUtilizadas = new Array(0);

function checkDatasPermitidas(data) {
	for (var i = 0; i < datasUtilizadas.length; i++) {
		if (datasUtilizadas[i] == data) {
			return true;
		}
	}
	return false;
}


/* ***************************************************************************************
 * SPINNER - SPINNER - SPINNER - SPINNER - SPINNER - SPINNER - SPINNER - SPINNER - SPINNER
 *****************************************************************************************/

function construirSpinners() {
	try {
		
		$('.spinner .btn:first-of-type').unbind('click');
		$('.spinner .btn:last-of-type').unbind('click');
		$('.spinner input').unbind('change');
		
		$('.spinner .btn:first-of-type').on('click', function(ev) {
			incrementar(getSpinner(ev), true);
		});
		
		$('.spinner .btn:last-of-type').on('click', function(ev) {
			incrementar(getSpinner(ev), false);
		});
		
		$('.spinner input').on('change', function(ev) {
			verificarSpinner(getSpinner(ev));
		});
		
	} catch (e) { alert('.spinner' + e); }
}

function getSpinner(ev) {
	var p = ev.target.parentNode.parentNode;
	var c = p.getElementsByTagName('input')[0];
	return $(c);
}

function verificarSpinner(input) {
	var max = parseInt(input.attr('data-max'));
	var min = parseInt(input.attr('data-min'));
	var valor = parseInt(input.val(), 10);
	
	if (!isNaN(max) && valor > max) {
		input.val(max).change();
	} else if (!isNaN(min) && valor < min) {
		input.val(min).change();
	}
}

function incrementar(input, up) {
	var step = parseInt(input.attr('data-step'));
	var max = parseInt(input.attr('data-max'));
	var min = parseInt(input.attr('data-min'));
	
	step = isNaN(step) ? 1 : parseInt(step);
	
	if (!up) step *= -1;
	
	var valor = parseInt(input.val(), 10);
	valor = isNaN(valor) ? 0 : valor;
	valor += step;
	
	if ((isNaN(max) || valor <= max) && (isNaN(min) || valor >= min)) {
		input.val(valor).change();
	}
}

/* ***************************************************************************************
 * ERROS / CONFIRMACAO - ERROS / CONFIRMACAO - ERROS / CONFIRMACAO - ERROS / CONFIRMACAO
 *****************************************************************************************/

function construirChamadaConfirmacao() {
	var as = $("a[data-confirma-mensagem]");
	for (var i = 0; i < as.length; i++ ) {
		var a = as[i];
		
		/*if ($(a).attr('href') == '#') { $(a).attr('href', '#...'); }*/
		
		if (!a.novoclick && $(a).attr('data-confirma-mensagem')) {
			a.novoclick = a.onclick;
			a.onclick = function() {
				var element = $(this)[0];
				bootbox.dialog({
					id: 'bootbox',
					message: '<i class="fa fa-exclamation-triangle alert-icon"/> ' + $(element).attr('data-confirma-mensagem'),
					title: 'Atenção!', animate: false, onEscape: true,
					buttons: {
						danger: { label: "Não", className: "btn-default" },
						main: { label: "Sim", className: "btn-danger", callback: function() { element.novoclick(); } }
					}
				});
				return false;
			};
		}
	}
}

function getMensagens() {
	return document.getElementById('mensagens-sistema').innerHTML;
}
function limparMensagens() {
	document.getElementById('mensagens-sistema').innerHTML = '';
}

function adicionarErro(mensagem) {
	document.getElementById('mensagens-sistema').innerHTML += '<li class="alert alert-danger alert-dismissable">' + mensagem + '</li>';
}

var haserrors = false;

function showErros() {
	var erros = getMensagens();
	if (erros != '') {
		modalClass = '';
		var btnclass = 'btn-default';
		//var btnclass = 'btn-outline';
		if (erros.indexOf('alert-success') != -1) {
			btnclass = 'alert-success';
			//modalClass = 'modal-success';
		} else if (erros.indexOf('alert-warning') != -1) {
			btnclass = 'alert-warning';
			//modalClass = 'modal-alert';
		} else if (erros.indexOf('alert-danger') != -1) {
			btnclass = 'alert-danger';
			//modalClass = 'modal-danger';
		}
		
		haserrors = btnclass == 'alert-warning' || btnclass == 'alert-danger';
		limparMensagens();
		bootbox.dialog({
			message: erros, backdrop: true, animate: false, onEscape: true,
			/*title: 'Atenção!', className: modalClass,*/
			buttons: { confirm: { label: '<i class="fa fa-remove"/> FECHAR', className: btnclass,
									callback: function(result) { haserrors = false; },} },
		});
	}
}

/* ***************************************************************************************
 * POPUPS - POPUPS - POPUPS - POPUPS - POPUPS - POPUPS - POPUPS - POPUPS - POPUPS - POPUPS
 *****************************************************************************************/

function empilharModais() {
	$('.modal').not('.ajaxmodal').on('show.bs.modal', function (event) {
		setTimeout(function() {
			var idx = $('.modal:visible').length;
			$(this).css('z-index', 2040 + (10 * idx));
		}, 500);
	});
	$('.modal').not('.ajaxmodal').on('shown.bs.modal', function (event) {
		setTimeout(function() {
			var idx = ($('.modal:visible').length) - 1;
			$('.modal-backdrop').not('.stacked').css('z-index', 2038 + (10 * idx));
			$('.modal-backdrop').not('.stacked').addClass('stacked');
		}, 500);
	});
}

function abrirAjaxModal(arguments) {
	abrirPopup('ajaxmodal', true, true);
}

function fecharAjaxModal() {
	fecharPopup('ajaxmodal');
	construirChamadaConfirmacao();
}

function abrirPopup(popupName, notupdate, notclicktoexit) {
	if (notupdate != true) {
		PrimeFaces.ab({source:'', u:"popupForm_" + popupName /*, f:"popupForm_" + popupName*/ });
	}
	
	if (popupName != 'ajaxmodal') {
		$(document.body).addClass('popup-modal-open')
	}
	
	if (notclicktoexit) {
		$("#" + popupName).modal({backdrop: 'static', keyboard: false});
	} else {
		$("#" + popupName).modal();
	}
}

function fecharPopup(popupName, checkerrors) {
	if (checkerrors && haserrors) {
		return;
	}
	if (popupName != 'ajaxmodal') {
		$(document.body).removeClass('popup-modal-open')
	}
	$("#" + popupName).modal("hide");
}

function abrirPopupPF(popupName, notupdate) {
	if (notupdate != true) {
    	PrimeFaces.ab({source:'', u:"popupForm_" + popupName});
	}
	PF(popupName).show();
}

function fecharPopupPF(popupName, checkerrors) {
	if (checkerrors && haserrors) {
		return;
	}
	PF(popupName).hide();
}

/* ***************************************************************************************
 * MASCARA - MASCARA - MASCARA - MASCARA - MASCARA - MASCARA - MASCARA - MASCARA - MASCARA
 *****************************************************************************************/

function mascararCampos() {
	$("[data-mask]").inputmask();
	
	$(".mask-cep").inputmask("99.999-999");
	$(".mask-cpf").inputmask("999.999.999-99");
	$(".mask-cnpj").inputmask("99.999.999/9999-99");
	$(".mask-date").inputmask("99/99/9999", { alias: "dd/mm/yyyy" });
	$(".mask-fone").inputmask({ mask: ["(99) 9999.9999", "(99) 9 9999.9999", ], keepStatic: true });
	$(".mask-cnpj-cpf").inputmask({ mask: ["999.999.999-99", "99.999.999/9999-99", ], keepStatic: true });
	
	attrPlaceholder(".mask-cnpj-cpf", "CNPJ / CPF");
	attrPlaceholder(".mask-cep", "99.999-999");
	attrPlaceholder(".mask-cpf", "999.999.999-99");
	attrPlaceholder(".mask-cnpj", "99.999.999/9999-99");
	attrPlaceholder(".mask-date", "dd/mm/aaaa");
	attrPlaceholder(".mask-fone", "(99) [9]9999.9999");
	
	attrPlaceholder("input[type=email]", "email@empresa.com.br");
}

function attrPlaceholder(element, placeholder) {
	$(element).attr("placeholder", function( i, val ) {
		if (val && val.indexOf(placeholder) > -1) {
			return val;
		}
        return (val ? val + " - " : "") + placeholder;
	});
}


function getSomenteNumero(texto) {
    if (!texto) { return 0.00; }
    var value = "";
    for (var i = 0; i < texto.length; i++) {
        var c = texto.charAt(i);
        if (!isNaN(c)) { value += c; }
    }
    if (value == "") { return "0000"; }
    return value;
}

function formataNumero(numero, scale) {
	scale = !scale || isNaN(scale) ? 2 : scale
    var value = "0";
    if (numero) { value = getSomenteNumero("" + numero); }  
    value = ("" + value).replace(new RegExp("\\.", "g"), "");
    
    if (value.length > scale) {
        value = value.replace(',', '');
        value = value.substring(0, value.length - scale) + ',' + value.substring(value.length - scale, value.length);
    } else {
        value = formataNumero("0" + value, scale);
    }
    
    while (value.substring(0, 1) == '0' && value.length > (2 + scale)) {
        value = value.substring(1, value.length);
    }
    
    return value;
}

function mascaraNumero(input, scale) {
    input.value = formataNumero(input.value, scale);
    return true;
}

/* ***************************************************************************************
 * GET ELEMENT - GET ELEMENT - GET ELEMENT - GET ELEMENT - GET ELEMENT - GET ELEMENT
 *****************************************************************************************/

function getElements(id) {
    var componentes = document.all;
    var inputs = new Array();
    for (var i = 0; i < componentes.length; i++) {
        var input = componentes[i];
        if (isSameId(input, id)) {
        	inputs[inputs.length] = input;
        }
    }
    return inputs;
}

function getElement(id) {
    var componentes = document.all;
    for (var i = 0; i < componentes.length; i++) {
        var input = componentes[i];
        if (isSameId(input, id)) {
            return input;
        }
    }
    return null;
}

function isSameId(input, id) {
    var nomes = input.id.split(':');
    return nomes[nomes.length-1] == id;
}

/* ***************************************************************************************
 * VALIDACAO - VALIDACAO - VALIDACAO - VALIDACAO - VALIDACAO - VALIDACAO - VALIDACAO
 *****************************************************************************************/

function validaCPF(cpf) {
	cpf = cpf.replace(/[^\d]+/g,'');    
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 ||  cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222"
    	|| cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666"
		|| cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)       
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)        
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;   
}

/* ***************************************************************************************
 * COLLAPSE DUAL PANELS - COLLAPSE DUAL PANELS - COLLAPSE DUAL PANELS
 *****************************************************************************************/

var collapseControlClass = "collapse-control";
var panelListCollapseClass = "panel-list-collapse";
var treeCollapseClass = "tree-collapse";
var faCollapseLeftClass = "fa-angle-double-left";
var faCollapseRightClass = "fa-angle-double-right";

function inicializarCollapsePanelTarefas() {
	$('.' + collapseControlClass).unbind('click');
	$('.' + collapseControlClass).click(function () {
		
		if ($('.' + panelListCollapseClass).hasClass(treeCollapseClass)) {
			$('.' + panelListCollapseClass).removeClass(treeCollapseClass)
			$('.' + collapseControlClass + ' .fa').addClass(faCollapseLeftClass).removeClass(faCollapseRightClass)
			
		} else {
			$('.' + panelListCollapseClass).addClass(treeCollapseClass)
			$('.' + collapseControlClass + ' .fa').addClass(faCollapseRightClass).removeClass(faCollapseLeftClass)
		}
	});
}

/* ***************************************************************************************
 * INICIALIZACAO - INICIALIZACAO - INICIALIZACAO - INICIALIZACAO - INICIALIZACAO
 *****************************************************************************************/

function reestilizarFileUpload() {
	$('.ui-fileupload-choose .ui-button-text.ui-c').html(
			'<i class="icon fa fa-upload"/>'
			+ '<p>Arraste e solte seus arquivos aqui<br/>'
			+ '<small>ou</small></p>'
			+ '<span class="btn btn-default btn-flat maiusculo">Procurar Arquivos</span>'
			);
}

function construirTooltip() {
	$('.tooltip').remove();
	$('body').tooltip({ selector: "[data-toggle='tooltip']", container: 'body', delay: {show: 250, hide: 100} });
}

function construirChecks() {
	$('input[type=radio]').iCheck({ radioClass: 'iradio_minimal', increaseArea: '20%' });
	$('input[type=radio]').on('ifChecked', function (ev) { ev.target.checked = true; $(ev.target).change(); });
	$('input[type=radio]').on('ifUnchecked', function (ev) { ev.target.checked = false; });
	
	$('input[type=checkbox]').iCheck({ checkboxClass: 'icheckbox_minimal', increaseArea: '20%' }); 
    $('input[type=checkbox]').on('ifChecked', function (ev) { ev.target.checked = true; $(ev.target).change(); });
    $('input[type=checkbox]').on('ifUnchecked', function (ev) { ev.target.checked = false; $(ev.target).change();  });
}

$(document).ready(function() {
	iniciar();
});

function iniciar() {
	desabilitarLinks();
    mascararCampos();
    construirTooltip();
    construirChecks();
    
    construirChamadaConfirmacao();
    construirCalendarios();
    construirSpinners();
    showErros();
    empilharModais();
    
    inicializarCollapsePanelTarefas();
    
    reestilizarFileUpload();
}

$.AdminLTE.options.enableBSTooltip = false;
$.AdminLTE.options.enableBSToppltip = false;


function log(text) {
	var log = document.getElementById('log-console');
	if (log) { log.innerHTML += "<br/>" + text; }
}
