package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Pagamento;
import br.com.seinfra.repository.query.PagamentoRepositoryQuery;

@Repository
public interface PagamentoRepository extends JpaRepository<Pagamento, Long>, PagamentoRepositoryQuery {

}
