package br.com.seinfra.security;

import javax.enterprise.inject.Produces;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import br.com.seinfra.model.PerfilPermissao;
import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.service.PermissaoService;
import br.com.seinfra.service.UsuarioService;

@Component("seguranca")
@Scope("request")
public class Seguranca {

	@Autowired
	private ExternalContext externalContext;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private PermissaoService permissaoService;
	
	public String getNomeUsuario() {
		String nome = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			nome = usuarioLogado.getUsuario().getNome();
		}
		
		return nome;
	}
	
	public String getEmail() {
		String email = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			email = usuarioLogado.getUsuario().getEmail();
		}
		
		return email;
	}
	
	public Boolean permissao(String role) {
		Boolean retorno = Boolean.FALSE;
		Permissao permissao = permissaoService.usuarioPermissao(role);
		Usuario usuario = usuarioService.usuarioPermissao(getNomeUsuario());
		
		for(PerfilPermissao perfil : usuario.getPerfil().getPermissoes()) {
			if(perfil.getPermissao().getRole().equals(permissao.getRole())) {
				retorno = Boolean.TRUE;
			}
		}
		return retorno;
	}

	@Produces
	@UsuarioLogado
	public UsuarioSistema getUsuarioLogado() {
		UsuarioSistema usuario = null;
		
		UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) 
				FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
		
		if (auth != null && auth.getPrincipal() != null) {
			usuario = (UsuarioSistema) auth.getPrincipal();
		}
		
		return usuario;
	}
	
	public boolean isEmitirPedidoPermitido() {
		return externalContext.isUserInRole("ADMINISTRADORES") 
				|| externalContext.isUserInRole("VENDEDORES");
	}
	
	public boolean isCancelarPedidoPermitido() {
		return externalContext.isUserInRole("ADMINISTRADORES") 
				|| externalContext.isUserInRole("VENDEDORES");
	}
	
}
