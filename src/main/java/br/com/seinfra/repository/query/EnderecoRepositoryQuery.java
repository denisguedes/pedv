package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Endereco;
import br.com.seinfra.model.filter.EnderecoFilter;

public interface EnderecoRepositoryQuery {

	public Page<Endereco> pequisar(EnderecoFilter filtro, Pageable pageable);
	
}
