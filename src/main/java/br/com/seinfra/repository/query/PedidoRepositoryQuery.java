package br.com.seinfra.repository.query;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.seinfra.model.Pedido;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.enums.Periodo;
import br.com.seinfra.model.filter.PedidoFilter;

public interface PedidoRepositoryQuery {

	public List<Pedido> pequisar(PedidoFilter filtro);
	
	public List<Pedido> verificaAgendamento(LocalDate dataInicio, LocalDate dataFim, Periodo periodo);
	
	public Long total(PedidoFilter filtro);
	
	public Map<Date, BigDecimal> valoresTotaisPorData(Integer numeroDeDias, Usuario criadoPor);
}
