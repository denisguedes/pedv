package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Endereco;
import br.com.seinfra.model.filter.EnderecoFilter;
import br.com.seinfra.repository.EnderecoRepository;

@Service
public class EnderecoService extends GenericBaseService<EnderecoRepository, Endereco, Long>{
	
	public Page<Endereco> pequisar(EnderecoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
}
