package br.com.seinfra.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.seinfra.model.Pagamento;
import br.com.seinfra.model.filter.PagamentoFilter;
import br.com.seinfra.repository.query.PagamentoRepositoryQuery;

public class PagamentoRepositoryImpl implements PagamentoRepositoryQuery {

	@Autowired
	private EntityManager manager;

	@Override
	public Page<Pagamento> pequisar(PagamentoFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pagamento> criteria = builder.createQuery(Pagamento.class);
		Root<Pagamento> root = criteria.from(Pagamento.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("numero")));

		TypedQuery<Pagamento> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(filtro));
	}

	private Predicate[] criarRestricoes(PagamentoFilter filtro, CriteriaBuilder builder,
			Root<Pagamento> root) {

		List<Predicate> predicates = new ArrayList<>();

		if(filtro.getId() != null) {			
			predicates.add(builder.equal(root.get("codigo"), filtro.getId()));
		}
		
		if (!StringUtils.isEmpty(filtro.getDataVencimento())) {
			predicates.add(builder.like(builder.lower(root.get("dataVencimento")),
					"%" + filtro.getDataVencimento() + "%"));
		}				

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);

	}

	private Long total(PagamentoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Pagamento> root = criteria.from(Pagamento.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
}
