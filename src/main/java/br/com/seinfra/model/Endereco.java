package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_endereco", schema = "bdestq")
public class Endereco extends GenericBaseModel<Long>{

	@Column(nullable = false, length = 150)
	private String logradouro;
	
	@Column(nullable = false, length = 20)
	private String numero;
	
	@Column(length = 150)
	private String complemento;
	
	@Column(nullable = false, length = 60)
	private String cidade;
	
	@Column(nullable = false, length = 60)
	private String uf;
	
	@Column(nullable = false, length = 9)
	private String cep;
	
	@ManyToOne
	@JoinColumn(name = "cliente_id", nullable = false)
	private Cliente cliente;

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}