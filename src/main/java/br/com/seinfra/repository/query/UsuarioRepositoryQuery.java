package br.com.seinfra.repository.query;

import java.util.List;

import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.filter.UsuarioFilter;

public interface UsuarioRepositoryQuery {
	
	public List<Usuario> pequisar(UsuarioFilter filtro);
	
	public Long total(UsuarioFilter filtro);
	
}
