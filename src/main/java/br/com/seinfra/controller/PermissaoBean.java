 package br.com.seinfra.controller;

import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Component;

import br.com.seinfra.global.controller.GenericBaseBean;
import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.filter.PermissaoFilter;
import br.com.seinfra.service.PermissaoService;

@ViewScoped
@Component("permissaoBean")
public class PermissaoBean extends GenericBaseBean<PermissaoService, Permissao, Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PermissaoFilter filtro;
	
	private LazyDataModel<Permissao> model;
	
	public PermissaoBean() {
		setTipo("Permissao");
		filtro = new PermissaoFilter();
		model = new LazyDataModel<Permissao>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Permissao> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
					Map<String, Object> filters) {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(servico.quantidadeFiltrados(filtro).intValue());
				
				return servico.pequisar(filtro);
			}
		};
	}

	public void inicializar() {
		if (getSelecionado() == null) {
			limpar();
		}
	}
	
	private void limpar() {
		selecionado = new Permissao();
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public PermissaoFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(PermissaoFilter filtro) {
		this.filtro = filtro;
	}

	public LazyDataModel<Permissao> getModel() {
		return model;
	}

}
