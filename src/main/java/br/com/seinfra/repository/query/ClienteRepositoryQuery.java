package br.com.seinfra.repository.query;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Cliente;
import br.com.seinfra.model.filter.ClienteFilter;

@Repository
public interface ClienteRepositoryQuery {

	public List<Cliente> pequisar(ClienteFilter filtro);
	
	public Long total(ClienteFilter filtro);
	
	public List<Cliente> buscarPorNome(String nome);
	
}
