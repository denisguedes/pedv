package br.com.seinfra.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_perfil_permissao", schema = "bdestq")
public class PerfilPermissao extends GenericBaseModel<Long> {

	@ManyToOne
	@JoinColumn(name = "perfil_id")
	@JsonBackReference("permissoes")
	private Perfil perfil;

	@ManyToOne
	@JoinColumn(name = "permissao_id")
	private Permissao permissao;
	
	public PerfilPermissao() {
		super();
	}

	public PerfilPermissao(Perfil perfil, Permissao permissao) {
		super();
		this.perfil = perfil;
		this.permissao = permissao;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

}
