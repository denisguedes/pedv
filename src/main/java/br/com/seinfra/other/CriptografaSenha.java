package br.com.seinfra.other;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CriptografaSenha {

	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		System.out.println(encoder.encode("mestre@admin"));
	}
	
	public String codificarSenha(String senha) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(senha);
	}
	
}
