package br.com.seinfra.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Perfil;
import br.com.seinfra.model.filter.PerfilFilter;
import br.com.seinfra.repository.PerfilRepository;

@Service
public class PerfilService  extends GenericBaseService<PerfilRepository, Perfil, Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5974133964061573838L;

	public List<Perfil> pequisar(PerfilFilter Filtro) {
		return repositorio.pequisar(Filtro);
	}
	
	public Long quantidadeFiltrados(PerfilFilter filtro) {
		return repositorio.total(filtro);
	}
	
	public List<Perfil> findAll(){
		return repositorio.findAll();
	}
	
}
