package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Pagamento;
import br.com.seinfra.model.filter.PagamentoFilter;

public interface PagamentoRepositoryQuery {

	public Page<Pagamento> pequisar(PagamentoFilter filtro, Pageable pageable);
	
}
