package br.com.seinfra.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Pagamento;
import br.com.seinfra.model.filter.PagamentoFilter;
import br.com.seinfra.repository.PagamentoRepository;

@Service
public class PagamentoService extends GenericBaseService<PagamentoRepository, Pagamento, Long>{
	
	public Page<Pagamento> pequisar(PagamentoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public List<Pagamento> parcelamento(Long codigo, BigDecimal valor, Long numero, LocalDate dataVencimento){
		BigDecimal valorParcela = valor.divide(BigDecimal.valueOf(numero));
		List<Pagamento> pagamentos = new ArrayList<>();
		
		for(int i = 1; i <= numero; i++) {
			Pagamento pagamento = new Pagamento();
			pagamento.setValorParcela(valorParcela);
	        pagamento.setValorPagamento(valor);
	        pagamento.setDataVencimento(dataVencimento);
	        if(i > 1) {
		        pagamento.setDataVencimento(dataVencimento.plusMonths(i-1));
	        }
	        pagamento.setNumero(i);
	        pagamentos.add(pagamento);
		}
		return pagamentos;
	}
}
