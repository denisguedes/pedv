package br.com.seinfra.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.seinfra.exception.NegocioException;
import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Produto;
import br.com.seinfra.model.filter.ProdutoFilter;
import br.com.seinfra.repository.ProdutoRepository;

@Service
public class ProdutoService  extends GenericBaseService<ProdutoRepository, Produto, Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1800213360572673181L;
	
	public List<Produto> pequisar(ProdutoFilter Filtro) {
		return repositorio.pequisar(Filtro);
	}
	
	public Long quantidadeFiltrados(ProdutoFilter filtro) {
		return repositorio.total(filtro);
	}
	
	@Transactional
	public Produto salvar(Produto produto) throws NegocioException {
		Produto produtoExistente = repositorio.porSku(produto.getSku());
		
		if (produtoExistente != null && !produtoExistente.equals(produto)) {
			throw new NegocioException("Já existe um produto com o SKU informado.");
		}
		
		return repositorio.save(produto);
	}
	
	public Produto porSku(String sku) {
		return repositorio.porSku(sku);
	}
	
	public List<Produto> porNome(String nome) {
		return repositorio.findByNome(nome);
	}
	
}
