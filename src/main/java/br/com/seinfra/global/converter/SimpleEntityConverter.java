package br.com.seinfra.global.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.seinfra.global.model.GenericBaseModel;
import br.com.seinfra.global.model.IGenericBaseModel;

@FacesConverter(forClass = GenericBaseModel.class, value = "GenericBaseModel")
public class SimpleEntityConverter implements Converter<Object> {
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        if (StringUtils.isNotEmpty(value)) {
            Object selected = component.getAttributes().get(value);
			GenericBaseModel entidade = (GenericBaseModel) selected;
        	if (entidade != null) {
	            entidade.setId(Long.valueOf(value));
	            return entidade;
        	}else {
        		return null;
        	}
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext ctx, UIComponent component, Object value) {
        if (value instanceof IGenericBaseModel) {
        	IGenericBaseModel entidade = (IGenericBaseModel) value;
            Serializable codigo = entidade.getId();
            if (codigo == null) {
                codigo = entidade.getClass();
            }
            if (codigo != null) {
                String valueOf = String.valueOf(codigo);
                component.getAttributes().put(valueOf, value);
                return valueOf;
            }
        }
        return (String) value;
    }
    
}
