 package br.com.seinfra.controller;

import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Component;

import br.com.seinfra.global.controller.GenericBaseBean;
import br.com.seinfra.model.Categoria;
import br.com.seinfra.model.filter.CategoriaFilter;
import br.com.seinfra.service.CategoriaService;

@ViewScoped
@Component("categoriaBean")
public class CategoriaBean extends GenericBaseBean<CategoriaService, Categoria, Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CategoriaFilter filtro;
	
	private LazyDataModel<Categoria> model;
	
	public CategoriaBean() {
		setTipo("Categoria");
		filtro = new CategoriaFilter();
		limpar();
		
		model = new LazyDataModel<Categoria>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Categoria> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
					Map<String, Object> filters) {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(servico.quantidadeFiltrados(filtro).intValue());
				
				return servico.pequisar(filtro);
			}
		};
	}

	public void inicializar() {
		if (getSelecionado() == null) {
			limpar();
		}
	}
	
   public String novo() {
    	limpar();
        return null;
    }
	
	private void limpar() {
		selecionado = new Categoria();
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public CategoriaFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(CategoriaFilter filtro) {
		this.filtro = filtro;
	}

	public LazyDataModel<Categoria> getModel() {
		return model;
	}

}
