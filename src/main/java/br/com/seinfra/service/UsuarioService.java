package br.com.seinfra.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.filter.UsuarioFilter;
import br.com.seinfra.other.CriptografaSenha;
import br.com.seinfra.repository.UsuarioRepository;

@Service
public class UsuarioService extends GenericBaseService<UsuarioRepository, Usuario, Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5170977744341326699L;

	public List<Usuario> pequisar(UsuarioFilter Filtro) {
		return repositorio.pequisar(Filtro);
	}
	
	public Long quantidadeFiltrados(UsuarioFilter filtro) {
		return repositorio.total(filtro);
	}
	
	public List<Usuario> vendedores(){
		return repositorio.findAll();
	}
	
	public Usuario usuarioPermissao(String nome) {
		return repositorio.findByNome(nome);
	}
	
	public Usuario porEmail(String email) {
		return repositorio.findByEmail(email);
	}
	
	@Override
	public Usuario incluir(Usuario entidade) {
		CriptografaSenha geradorSenha = new CriptografaSenha();
		entidade.setSenha(geradorSenha.codificarSenha(entidade.getSenha()));
		return super.incluir(entidade);
	}
	
	@Override
	public Usuario alterar(Long codigo, Usuario entidade) {
		
		Usuario usuario = buscarPorCodigo(codigo);
		
		if(usuario.getSenha().equals(entidade.getSenha())) {
			return super.alterar(codigo, entidade);
		}
		
		CriptografaSenha geradorSenha = new CriptografaSenha();
		entidade.setSenha(geradorSenha.codificarSenha(entidade.getSenha()));
		return super.alterar(codigo, entidade);
	}
}
