package br.com.seinfra.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Categoria;
import br.com.seinfra.model.filter.CategoriaFilter;
import br.com.seinfra.repository.CategoriaRepository;

@Service
public class CategoriaService  extends GenericBaseService<CategoriaRepository, Categoria, Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5974133964061573838L;

	public List<Categoria> pequisar(CategoriaFilter Filtro) {
		return repositorio.pequisar(Filtro);
	}
	
	public Long quantidadeFiltrados(CategoriaFilter filtro) {
		return repositorio.total(filtro);
	}
	
	public List<Categoria> listaCategorias(){
		return repositorio.findAll();
	}
	
}
