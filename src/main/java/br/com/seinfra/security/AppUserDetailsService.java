package br.com.seinfra.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.com.seinfra.model.PerfilPermissao;
import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.repository.PermissaoRepository;
import br.com.seinfra.repository.UsuarioRepository;

public class AppUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PermissaoRepository permissaoRepository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findByEmail(email);
		
		if(email.equals("administrador")) {
			return usuarioMestre(email);
		}
		
		UsuarioSistema user = null;
		
		if (usuario != null) {
			user = new UsuarioSistema(usuario, getPermissoes(usuario));
		} else {
			throw new UsernameNotFoundException("Usuário não encontrado.");
		}
		return user;
	}

	private Collection<? extends GrantedAuthority> getPermissoes(Usuario usuario) {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		
		Set<PerfilPermissao> permissoesPerfil = usuario.getPerfil().getPermissoes();
		permissoesPerfil.forEach(p -> authorities.add(new SimpleGrantedAuthority("ROLE_" + p.getPermissao().getRole().toUpperCase())));
		return authorities;
	}
	
	private UserDetails usuarioMestre(String login) throws UsernameNotFoundException {
		
		Usuario usuario = new Usuario();
		usuario.setEmail("administrador");
		usuario.setSenha("$2a$10$rmaoICl3ur4AAxI2TACAH.8tKT3ZcSwe/7ha9hbe0.suwj.r7C0q2"); //mestre@admin
		
		return new UsuarioSistema(usuario, getAllPermissoes());
	}
	
	private Collection<? extends GrantedAuthority> getAllPermissoes() {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		List<Permissao> permissoes = permissaoRepository.findAll();
		permissoes.forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getRole().toUpperCase())));		
		return authorities;
	}
	
}
