package br.com.seinfra.model.filter;

import java.io.Serializable;

import br.com.seinfra.exception.SKU;
import br.com.seinfra.global.model.GenericBaseFilter;

public class ProdutoFilter extends GenericBaseFilter<Serializable>{

	@SKU(message = "Por favor, informe um SKU no formato XX9999")
	private String sku;
	private String nome;
	
	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku == null ? null : sku.toUpperCase();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}