package br.com.seinfra.global.service;

import java.util.List;

import br.com.seinfra.global.model.IGenericBaseModel;

public interface IGenericBaseService<E extends IGenericBaseModel, T> {

	E incluir(E entidade);
	E alterar(Long id, E entidade);
	void excluir(E entidade);
	E buscarPorCodigo(T id);
	Object listar();
	List<E> todos();
		
}
