package br.com.seinfra.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.seinfra.config.jsf.FacesUtil;
import br.com.seinfra.exception.NegocioException;
import br.com.seinfra.exception.SKU;
import br.com.seinfra.global.controller.GenericBaseBean;
import br.com.seinfra.model.Cliente;
import br.com.seinfra.model.EnderecoEntrega;
import br.com.seinfra.model.ItemPedido;
import br.com.seinfra.model.Pedido;
import br.com.seinfra.model.Produto;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.enums.FormaPagamento;
import br.com.seinfra.model.enums.StatusPedido;
import br.com.seinfra.model.filter.PedidoFilter;
import br.com.seinfra.service.ClienteService;
import br.com.seinfra.service.PedidoService;
import br.com.seinfra.service.ProdutoService;
import br.com.seinfra.service.UsuarioService;

@ViewScoped
@Component("pedidoBean")
public class PedidoBean extends GenericBaseBean<PedidoService, Pedido, Long> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private UsuarioService usuarioaService;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private ClienteService clienteService;
	
	private String sku;
	
	private List<Usuario> vendedores;
	
	private Produto produtoLinhaEditavel;
	
	private PedidoFilter filtro;
	
	private LazyDataModel<Pedido> model;
	
	  @PostConstruct
	    public void init() {
			selecionado = new Pedido();
	    }
	
	public PedidoBean() {
		setTipo("Pedido");
		filtro = new PedidoFilter();
		model = new LazyDataModel<Pedido>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Pedido> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
					Map<String, Object> filters) {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				
				setRowCount(servico.quantidadeFiltrados(filtro).intValue());
				
				return servico.pequisar(filtro);
			}
			
		};
	}
	
	public void inicializar() {
		if (this.getSelecionado() == null) {
			limpar();
		}
		
		this.vendedores = this.usuarioaService.vendedores();
		this.getSelecionado().adicionarItemVazio();
		this.recalcularPedido();
	}
	
	public void clienteSelecionado(SelectEvent event) {
		getSelecionado().setCliente((Cliente) event.getObject());
	}
	
	private void limpar() {
		selecionado = new Pedido();
		getSelecionado().setEnderecoEntrega(new EnderecoEntrega());
	}
	
	public void enviarPedido() {
	}
	
	public void novo() {
		inicializar();
		limpar();
	}
	
	public String salvar() {
		this.getSelecionado().removerItemVazio();
		
		try {
			this.selecionado = servico.salvar(this.selecionado);
		
			FacesUtil.addInfoMessage("Pedido salvo com sucesso!");
		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		} finally {
			this.selecionado.adicionarItemVazio();
		}
		return "pesquisa";
	}
	
	public void recalcularPedido() {
		if (this.getSelecionado() != null) {
			this.getSelecionado().recalcularValorTotal();
		}
	}
	
	public void carregarProdutoPorSku() {
		if (StringUtils.isNotEmpty(this.sku)) {
			this.produtoLinhaEditavel = this.produtoService.porSku(this.sku);
			this.carregarProdutoLinhaEditavel();
		}
	}
	
	public void carregarProdutoLinhaEditavel() {
		ItemPedido item = this.getSelecionado().getItens().iterator().next();
		
		if (this.produtoLinhaEditavel != null) {
			if (this.existeItemComProduto(this.produtoLinhaEditavel)) {
				FacesUtil.addErrorMessage("Já existe um item no pedido com o produto informado.");
			} else {
				item.setProduto(this.produtoLinhaEditavel);
				item.setValorUnitario(this.produtoLinhaEditavel.getValorUnitario());
				
				this.getSelecionado().adicionarItemVazio();
				this.produtoLinhaEditavel = null;
				this.sku = null;
				
				this.getSelecionado().recalcularValorTotal();
			}
		}
	}
	
	private boolean existeItemComProduto(Produto produto) {
		boolean existeItem = false;
		
		for (ItemPedido item : this.getSelecionado().getItens()) {
			if (produto.equals(item.getProduto())) {
				existeItem = true;
				break;
			}
		}
		return existeItem;
	}

	public List<Produto> completarProduto(String nome) {
		return this.produtoService.porNome(nome);
	}
	
	public void atualizarQuantidade(ItemPedido item, int linha) {
		if (item.getQuantidade() < 1) {
			if (linha == 0) {
				item.setQuantidade(1);
			} else {
				this.getSelecionado().getItens().remove(item);
			}
		}
		
		this.selecionado.recalcularValorTotal();
	}
	
	public FormaPagamento[] getFormasPagamento() {
		return FormaPagamento.values();
	}
	
	public List<Cliente> completarCliente(String nome) {
		return this.clienteService.buscarPorNome(nome);
	}
	
	public void posProcessarXls(Object documento) {
		HSSFWorkbook planilha = (HSSFWorkbook) documento;
		HSSFSheet folha = planilha.getSheetAt(0);
		HSSFRow cabecalho = folha.getRow(0);
		HSSFCellStyle estiloCelula = planilha.createCellStyle();
		Font fonteCabecalho = planilha.createFont();
		
		fonteCabecalho.setColor(IndexedColors.WHITE.getIndex());
		fonteCabecalho.setBold(true);
		fonteCabecalho.setFontHeightInPoints((short) 16);
		
		estiloCelula.setFont(fonteCabecalho);
		estiloCelula.setFillForegroundColor(IndexedColors.BLACK.getIndex());
		estiloCelula.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		
		for (int i = 0; i < cabecalho.getPhysicalNumberOfCells(); i++) {
			cabecalho.getCell(i).setCellStyle(estiloCelula);
		}
	}
	
	public void emitirPedido() {
		this.selecionado.removerItemVazio();
		
		try {
			this.selecionado = this.servico.emitir(this.selecionado);
			
			FacesUtil.addInfoMessage("Pedido emitido com sucesso!");
		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		} finally {
			this.selecionado.adicionarItemVazio();
		}
	}
	
	public void cancelarPedido() {
		try {
			this.selecionado = this.servico.cancelar(this.selecionado);
			
			FacesUtil.addInfoMessage("Pedido cancelado com sucesso!");
		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		}
	}
	
	public StatusPedido[] getStatuses() {
		return StatusPedido.values();
	}
	
	public PedidoFilter getFiltro() {
		return filtro;
	}

	public LazyDataModel<Pedido> getModel() {
		return model;
	}

	public List<Usuario> getVendedores() {
		return vendedores;
	}
	
	public boolean isEditando() {
		return getSelecionado() != null && getSelecionado().getId() != null;
	}

	public Produto getProdutoLinhaEditavel() {
		return produtoLinhaEditavel;
	}

	public void setProdutoLinhaEditavel(Produto produtoLinhaEditavel) {
		this.produtoLinhaEditavel = produtoLinhaEditavel;
	}

	@SKU
	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}
	
	@NotBlank
	public String getNomeCliente() {
		return getSelecionado() != null && getSelecionado().getCliente() != null ? getSelecionado().getCliente().getNome() : null;
	}
	
	public void setNomeCliente(String nome) {
	}
}
