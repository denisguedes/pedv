package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Permissao;
import br.com.seinfra.repository.query.PermissaoRepositoryQuery;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Long>, PermissaoRepositoryQuery {

	public Permissao findByRole(String role);
}
