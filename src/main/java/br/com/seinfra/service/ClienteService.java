package br.com.seinfra.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Cliente;
import br.com.seinfra.model.filter.ClienteFilter;
import br.com.seinfra.repository.ClienteRepository;

@Service
public class ClienteService extends GenericBaseService<ClienteRepository, Cliente, Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1800213360572673181L;

	public List<Cliente> pequisar(ClienteFilter Filtro) {
		return repositorio.pequisar(Filtro);
	}
	
	public Long quantidadeFiltrados(ClienteFilter filtro) {
		return repositorio.total(filtro);
	}
	
	public List<Cliente> buscarPorNome(String nome){
		return repositorio.buscarPorNome(nome);
	}
}
