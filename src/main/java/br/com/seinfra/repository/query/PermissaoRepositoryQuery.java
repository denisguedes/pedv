package br.com.seinfra.repository.query;

import java.util.List;

import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.filter.PermissaoFilter;

public interface PermissaoRepositoryQuery {

	public List<Permissao> pequisar(PermissaoFilter filtro);
	
	public Long total(PermissaoFilter filtro);
	
}
