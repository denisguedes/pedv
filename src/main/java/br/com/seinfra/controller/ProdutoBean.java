package br.com.seinfra.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.seinfra.global.controller.GenericBaseBean;
import br.com.seinfra.model.Categoria;
import br.com.seinfra.model.Produto;
import br.com.seinfra.model.filter.ProdutoFilter;
import br.com.seinfra.service.CategoriaService;
import br.com.seinfra.service.ProdutoService;

@ViewScoped
@Component("produtoBean")
public class ProdutoBean extends GenericBaseBean<ProdutoService, Produto, Long>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private CategoriaService categoriaService;
	
	private ProdutoFilter filtro;
	
	private Categoria categoria;
	
	private List<Categoria> categoriasRaizes;
	
	private LazyDataModel<Produto> model;
	
	public ProdutoBean() {
		setTipo("Produto");
		filtro = new ProdutoFilter();
		limpar();

		model = new LazyDataModel<Produto>() {
		private static final long serialVersionUID = 1L;
			
			@Override
			public List<Produto> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
					Map<String, Object> filters) {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(servico.quantidadeFiltrados(filtro).intValue());
				
				return servico.pequisar(filtro);
			}
		};
	}
	
	@PostConstruct
	public void inicializar() {
		if (this.getSelecionado() == null) {
			limpar();
		}
		categoriasRaizes = categoriaService.listaCategorias();
	}
	
	public void novo() {
		inicializar();
		limpar();
	}
	
	private void limpar() {
		this.selecionado = new Produto();
		categoria = null;
	}
	
	public String editar(Produto produto) {
		return "cadastro.xhtml?produto=" + getSelecionado().getId();
	}
	
	public ProdutoFilter getFiltro() {
		return filtro;
	}
	
	public List<Categoria> getCategoriasRaizes() {
		return categoriasRaizes;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public LazyDataModel<Produto> getModel() {
		return model;
	}

	public void setModel(LazyDataModel<Produto> model) {
		this.model = model;
	}

}
