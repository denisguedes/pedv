package br.com.seinfra.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_perfil", schema = "bdestq")
public class Perfil extends GenericBaseModel<Long>{

	@Column(nullable=false, length=40)
	private String nome;
	
	@Column(nullable=false, length=80)
	private String descricao;
	
	@JsonManagedReference("permissoes")
	@OneToMany(mappedBy = "perfil", cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)	
	private Set<PerfilPermissao> permissoes = new HashSet<>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<PerfilPermissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Set<PerfilPermissao> permissoes) {
		this.permissoes = permissoes;
	}

}