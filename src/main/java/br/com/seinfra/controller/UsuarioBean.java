 package br.com.seinfra.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.seinfra.global.controller.GenericBaseBean;
import br.com.seinfra.model.Perfil;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.filter.UsuarioFilter;
import br.com.seinfra.service.PerfilService;
import br.com.seinfra.service.UsuarioService;

@ViewScoped
@Component("usuarioBean")
public class UsuarioBean extends GenericBaseBean<UsuarioService, Usuario, Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UsuarioFilter filtro;
	
	@Autowired
	private PerfilService perfilService;
	
	private List<Perfil> perfis;
	
	private Perfil perfil;
	
	private LazyDataModel<Usuario> model;
	
	@PostConstruct
    public void init() {
		perfis = perfilService.findAll();
    }
	
	public UsuarioBean() {
		setTipo("Usuario");
		filtro = new UsuarioFilter();
		model = new LazyDataModel<Usuario>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Usuario> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
					Map<String, Object> filters) {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(servico.quantidadeFiltrados(filtro).intValue());
				
				return servico.pequisar(filtro);
			}
		};
	}

	public void inicializar() {
		if (getSelecionado() == null) {
			limpar();
		}
		perfis = perfilService.findAll();
	}
	
	private void limpar() {
		selecionado = new Usuario();
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public UsuarioFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(UsuarioFilter filtro) {
		this.filtro = filtro;
	}

	public LazyDataModel<Usuario> getModel() {
		return model;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

}
