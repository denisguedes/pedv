package br.com.seinfra.global.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.seinfra.config.jpa.Transactional;
import br.com.seinfra.config.jsf.FacesUtil;
import br.com.seinfra.global.model.IGenericBaseModel;
import br.com.seinfra.global.service.IGenericBaseService;

public abstract class GenericBaseBean<S extends IGenericBaseService<E, T>, E extends IGenericBaseModel, T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1474689354710001522L;

	@Autowired
	protected S servico;
	
	protected E selecionado;
	
	protected String tipo;
	
	public GenericBaseBean() {
		super();
	}
	
	@Transactional
	public String salvar() {
		try {
			selecionado = servico.incluir(selecionado);
			
			FacesUtil.addInfoMessage("Registrado com sucesso!");
		} catch (Exception ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		}
		return "pesquisa" + tipo;
	}
	
	public String editar() {
		return "cadastro?" + tipo + "=" + getSelecionado().getId();
	}
	
	@Transactional
	public void excluir(E selecionado) {
		try {
			servico.excluir(selecionado);
			
			FacesUtil.addInfoMessage("Registro " + selecionado.getId() 
					+ " excluído com sucesso.");
		} catch (Exception ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		}
	}
	
	public void selecionar(E model) {
	     setSelecionado(model);
	}

	public E getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(E selecionado) {
		this.selecionado = selecionado;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}