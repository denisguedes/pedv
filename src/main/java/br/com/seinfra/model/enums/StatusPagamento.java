package br.com.seinfra.model.enums;

public enum StatusPagamento {

	PENDENTE("Pendente"),
	CANCELADO("Cancelado"),
	PAGO("Pago");
	
	private final String descricao;
	
	StatusPagamento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
