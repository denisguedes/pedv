package br.com.seinfra.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.seinfra.exception.NegocioException;
import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.ItemPedido;
import br.com.seinfra.model.Pedido;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.enums.Periodo;
import br.com.seinfra.model.enums.StatusPedido;
import br.com.seinfra.model.filter.PedidoFilter;
import br.com.seinfra.repository.PedidoRepository;

@Service
public class PedidoService extends GenericBaseService<PedidoRepository, Pedido, Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1800213360572673181L;
	
	public List<Pedido> pequisar(PedidoFilter Filtro) {
		return repositorio.pequisar(Filtro);
	}
	
	public Long quantidadeFiltrados(PedidoFilter filtro) {
		return this.repositorio.total(filtro);
	}
	
	public List<Pedido> verificaAgendamento(LocalDate dataInicio, LocalDate dataFim, Periodo periodo) {
		return repositorio.verificaAgendamento(dataInicio, dataFim, periodo);
	}
	
	@Transactional
	public Pedido salvar(Pedido pedido) throws NegocioException {
		if (pedido.isNovo()) {
			pedido.setDataCriacao(new Date());
			pedido.setStatus(StatusPedido.ORCAMENTO);
		}
		
		pedido.recalcularValorTotal();
		
		if (pedido.isNaoAlteravel()) {
			throw new NegocioException("Pedido não pode ser alterado no status "
					+ pedido.getStatus().getDescricao() + ".");
		}
		
		if (pedido.getItens().isEmpty()) {
			throw new NegocioException("O pedido deve possuir pelo menos um item.");
		}
		
		if (pedido.isValorTotalNegativo()) {
			throw new NegocioException("Valor total do pedido não pode ser negativo.");
		}
		
		pedido = this.repositorio.save(pedido);
		return pedido;
	}
	
	@Transactional
	public Pedido emitir(Pedido pedido) throws NegocioException {
		pedido = this.repositorio.save(pedido);
		
		if (pedido.isNaoEmissivel()) {
			throw new NegocioException("Pedido não pode ser emitido com status "
					+ pedido.getStatus().getDescricao() + ".");
		}
		this.baixarItensEstoque(pedido);
		
		pedido.setStatus(StatusPedido.EMITIDO);
		pedido = this.repositorio.save(pedido);
		
		return pedido;
	}
	
	@Transactional
	public Pedido cancelar(Pedido pedido) throws NegocioException {
		pedido = this.repositorio.getOne(pedido.getId());
		
		if (pedido.isNaoCancelavel()) {
			throw new NegocioException("Pedido não pode ser cancelado no status "
					+ pedido.getStatus().getDescricao() + ".");
		}
		
		if (pedido.isEmitido()) {
			this.retornarItensEstoque(pedido);
		}
		
		pedido.setStatus(StatusPedido.CANCELADO);
		pedido = this.repositorio.save(pedido);
		
		return pedido;
	}
	
	@Transactional
	public void baixarItensEstoque(Pedido pedido) throws NegocioException {
		pedido = this.repositorio.getOne(pedido.getId());
		
		for (ItemPedido item : pedido.getItens()) {
			item.getProduto().baixarEstoque(item.getQuantidade());
		}
	}

	public void retornarItensEstoque(Pedido pedido) {
		pedido = this.repositorio.getOne(pedido.getId());
		
		for (ItemPedido item : pedido.getItens()) {
			item.getProduto().adicionarEstoque(item.getQuantidade());
		}
	}
	
	public Map<Date, BigDecimal> valoresTotaisPorData(Integer numeroDeDias, Usuario criadoPor){
		return this.repositorio.valoresTotaisPorData(numeroDeDias, criadoPor);
	}
}
